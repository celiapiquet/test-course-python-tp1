from django.apps import AppConfig


class TodolistprojectConfig(AppConfig):
    name = 'ToDoListProject'
