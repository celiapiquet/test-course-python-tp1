import datetime


# TEST IS OK
class EmailService:
    def send_if_possible(self, user):
        if self.is_age_above_18(user):
            return "Email sent"
        return "Email not sent"

    @staticmethod
    def is_age_above_18(user):
        age = abs(datetime.date.today() - user.birthdate).days // 365
        if age >= 18:
            return True
        return False
