from ToDoListProject.models import User, ToDoList


class UserService:

    @staticmethod
    def create_one_list_for_user(user):
        user_list = ToDoList(user=user)
        user_exist = UserService.get_user_by_id(user.id)
        list_of_the_user = UserService.get_list_by_user_id(user.id)
        if user_exist and not list_of_the_user:
            user_list.save()
            return user_list
        return None

    @staticmethod
    def create_user(user_dto):
        if user_dto.is_valid():
            user_dto.save()
            return user_dto
        return None

    @staticmethod
    def get_user_by_id(user_id):
        return User.objects.get(id=user_id)

    @staticmethod
    def get_list_by_user_id(user_id):
        return ToDoList.objects.get(userId=user_id)
