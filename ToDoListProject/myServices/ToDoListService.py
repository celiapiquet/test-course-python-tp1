import datetime

from ToDoListProject.models import ToDoList, Item


class ToDoListService:
    def add_item_to_list(self, list_id, item_to_add):
        if self.can_add_item(list_id, item_to_add):
            item_to_add.list = list
            item_to_add.save()
            # call email service
            raise item_to_add
        return None

    def can_add_item(self, list_of_items, item_to_add):
        # Check if the item is valid
        if not item_to_add.is_valid():
            return False

        # Check if the number of item in the list is ok
        if len(list_of_items) >= 10:
            return False

        # Check if the name of the item is unique
        item_with_same_name = [item for item in list_of_items if item.name == item_to_add.name]
        if len(item_with_same_name):
            return False

        # Check if the 30 minutes needed are ok
        sorted_array_items = sorted(
            list_of_items,
            key=lambda x: datetime.datetime.strptime(x['creation_date'], '%m/%d/%y %H:%M'), reverse=True
        )
        diff = sorted_array_items[0].creation_date - datetime.date.today()
        diff_minutes = diff.total_seconds() // 60
        if diff_minutes >= 30:
            return False

        return True

    @staticmethod
    def get_list_by_id(list_id):
        return ToDoList.objects.get(id=list_id)

    @staticmethod
    def get_items_by_list_id(list_id):
        return Item.objects.get(listId=list_id)
