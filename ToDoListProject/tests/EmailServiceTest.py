import datetime
from unittest.mock import patch

from django.test import TestCase

from ToDoListProject.myServices.EmailService import EmailService


class EmailServiceTest(TestCase):

    def setUp(self):
        self.email_service = EmailService()

    """
-------------------------------------------------------------------------------------------------------------------
    Test the send function (2 cases):
    - we mock the behavior of the function 'is_age_above_18' because the send method depend on it
    - we mock a user
-------------------------------------------------------------------------------------------------------------------
    """

    """
    Case 1 : nominal case the user is above 18
    """
    @patch('ToDoListProject.models.User')
    @patch('ToDoListProject.myServices.EmailService.EmailService.is_age_above_18')
    def test_send_nominal_case(self, mock_is_above_18, mock_user):
        mock_is_above_18.return_value = True
        self.assertEqual(self.email_service.send_if_possible(mock_user), "Email sent")

    """
    Case 2 : send method should not return True when the user is under 18
    """
    @patch('ToDoListProject.models.User')
    @patch('ToDoListProject.myServices.EmailService.EmailService.is_age_above_18')
    def test_send_not_above_18(self, mock_is_above_18, mock_user):
        mock_is_above_18.return_value = False
        self.assertEqual(self.email_service.send_if_possible(mock_user), "Email not sent")


    """
-------------------------------------------------------------------------------------------------------------------
    Test 'is_above_18' function (2 cases):
-------------------------------------------------------------------------------------------------------------------
    """

    """
    Case 1 : nominal case the user is above 18
    """
    @patch('ToDoListProject.models.User')
    def test_is_above_18_nominal_case(self, mock_user):
        mock_user.birthdate = datetime.date(2000, 1, 1)
        self.assertTrue(EmailService.is_age_above_18(mock_user))

    """
    Case 2 : return False when a user is under 18
    """
    @patch('ToDoListProject.models.User')
    def test_is_above_18_user_younger(self, mock_user):
        mock_user.birthdate = datetime.date.today()
        self.assertFalse(EmailService.is_age_above_18(mock_user))

