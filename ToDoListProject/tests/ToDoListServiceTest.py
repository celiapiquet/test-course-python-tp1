import datetime
from unittest.mock import patch, Mock

from django.test import TestCase

from ToDoListProject.models import ToDoList, Item
from ToDoListProject.myServices.ToDoListService import ToDoListService

"""
No time to complete the implementation of this TestCase, sorry..
"""


class ToDoListServiceTest(TestCase):

    def setUp(self):
        self.to_do_list_service = ToDoListService()

    def test_can_add_item_nominal_case(self):
        list = Mock(spec=ToDoList)
        list.id = 1
        list._state = Mock()
        item_added_1 = Item(content="This is a content 1", name="This is name 1", creation_date=datetime.datetime(
            2020, 5, 12, 14, 13, 23), list=list)
        item_added_2 = Item(content="This is a content 2", name="This is name 2", creation_date=datetime.datetime(
            2020, 6, 13, 22, 1, 12), list=list)
        list_items = []
        list_items.append(item_added_1)
        list_items.append(item_added_2)
        to_add = Item(content="This is a content 3", name="This is name 3", creation_date=datetime.date.today(),
                      list=list)
        self.assertTrue(self.to_do_list_service.can_add_item(list_items, to_add))

    def test_email_sent_when_item_added_for_user_above_18(self):
        raise NotImplementedError

    def test_email_not_sent_when_item_added_for_user_under_18(self):
        raise NotImplementedError

    def test_cannot_add_item_without_unique_name(self):
        raise NotImplementedError

    def test_cannot_add_item_when_30_minutes_are_not_respected(self):
        raise NotImplementedError

    def test_cannot_add_item_when_list_has_10_items_or_plus(self):
        raise NotImplementedError
