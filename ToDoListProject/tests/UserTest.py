import datetime
import unittest

from django.test import TestCase

from ToDoListProject.models import User


class UserTest(TestCase):
    def setUp(self):
        self.user = User(firstname="Toto", lastname="Tata", password="totototo", birthdate=datetime.date(2000, 1, 1),
                         email="toto@tata.fr")

    def test_nominal_case(self):
        self.assertTrue(self.user.is_valid())

    def test_firstname_missing(self):
        self.user.firstname = None
        self.assertFalse(self.user.is_valid())
        self.user.firstname = "Toto"

    def test_lastname_missing(self):
        self.user.lastname = None
        self.assertFalse(self.user.is_valid())
        self.user.lastname = "Tata"

    def test_password_missing(self):
        self.user.password = None
        self.assertFalse(self.user.is_valid())
        self.user.password = "totototo"

    def test_email_missing(self):
        self.user.email = None
        self.assertFalse(self.user.is_valid())
        self.user.email = "toto@tata.fr"

    def test_password_length_too_short(self):
        self.user.password = "t"
        self.assertFalse(self.user.is_valid())
        self.user.password = "totototo"

    def test_password_length_too_long(self):
        self.user.password = "to" * 40
        self.assertFalse(self.user.is_valid())
        self.user.password = "totototo"

    def test_user_too_young(self):
        self.user.birthdate = datetime.date.today()
        self.assertFalse(self.user.is_valid())
        self.user.birthdate = datetime.date(2000, 1, 1)

    def test_user_email_not_good_format(self):
        self.user.email = "cdebfrfrez"
        self.assertFalse(self.user.is_valid())
        self.user.email = "toto@tata.fr"
