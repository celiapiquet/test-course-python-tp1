from unittest.mock import Mock

from django.test import TestCase

from ToDoListProject.models import Item, ToDoList


class ItemTest(TestCase):
    def setUp(self):
        list = Mock(spec=ToDoList)
        list._state = Mock()
        self.item = Item(content="this is the content", name="Item 1", list=list)

    def test_is_valid_nominal_case(self):
        self.assertTrue(self.item.is_valid())

    def test_is_valid_content_missing(self):
        self.item.content = None
        self.assertFalse(self.item.is_valid())
        self.item.content = "this is the content"

    def test_is_valid_name_missing(self):
        self.item.name = None
        self.assertFalse(self.item.is_valid())
        self.item.name = "Item 1"
