import datetime
from unittest import mock
from unittest.mock import patch, Mock

from django.test import TestCase

from ToDoListProject.models import User, ToDoList
from ToDoListProject.myServices.UserService import UserService


class UserServiceTest(TestCase):

    def setUp(self):
        self.user_service = UserService()

    """
-------------------------------------------------------------------------------------------------------------------
    Test the creation of a user (2 cases)
    - mocking the is_valid method
-------------------------------------------------------------------------------------------------------------------
    """

    """
    Case 1 : the user is valid must return True
    """
    @patch('ToDoListProject.models.User.is_valid')
    def test_create_nominal_case(self, mock_is_valid):
        new_user = User(firstname="tata", lastname="toto", birthdate=datetime.date.today(), email="tata@toto.fr",
                        password="rgsgresgrg")
        mock_is_valid.return_value = True
        user = self.user_service.create_user(new_user)
        self.assertEqual(new_user, user)


    """
    Case 2 : the user is not valid must return False
    """

    @patch('ToDoListProject.models.User.is_valid')
    def test_create_user_invalid(self, mock_is_valid):
        new_user = User(lastname="toto", birthdate=datetime.date.today(), email="tata@toto.fr",
                        password="rgsgresgrg")
        mock_is_valid.return_value = False
        user = self.user_service.create_user(new_user)
        self.assertIsNone(user)

    """
-------------------------------------------------------------------------------------------------------------------
    Test the creation of a Todo List for a user (3 cases)
-------------------------------------------------------------------------------------------------------------------
    """

    """
    Case 1 : creation possible when user found and no list for the user
    """
    @patch('ToDoListProject.myServices.UserService.UserService.get_user_by_id')
    @patch('ToDoListProject.myServices.UserService.UserService.get_list_by_user_id')
    def test_create_new_to_do_list_possible(self, mock_get_list_by_user_id, mock_get_user_by_id):
        mock_get_list_by_user_id.return_value = False  # no list already exist
        mock_get_user_by_id.return_value = True  # the user exists
        # without mocking because I had some issues with mock and database
        u2 = User(firstname="Toto", lastname="Tata", password="totototo", birthdate=datetime.date(2000, 1, 1),
                         email="toto@tata.fr")
        u2.save()
        user_list = self.user_service.create_one_list_for_user(u2)
        self.assertEqual(user_list.user.id, u2.id) # we check if the id of the user is good and if user_list has an id
        self.assertIsNotNone(user_list.id)

    """
    Case 2 : creation not possible when user doesn't exists
    """
    @patch('ToDoListProject.myServices.UserService.UserService.get_user_by_id')
    @patch('ToDoListProject.myServices.UserService.UserService.get_list_by_user_id')
    def test_create_list_not_possible_because_no_user(self, mock_get_list_by_user_id, mock_get_user_by_id):
        mock_get_list_by_user_id.return_value = False  # no list already exist
        mock_get_user_by_id.return_value = False  # the user doesn't exist
        # without mocking because I had some issues with user's mock and database
        u2 = User(firstname="Toto", lastname="Tata", password="totototo", birthdate=datetime.date(2000, 1, 1),
                  email="toto@tata.fr")
        u2.save()
        user_list = self.user_service.create_one_list_for_user(u2)
        self.assertIsNone(user_list)
    """
    Case 3 : creation not possible when the user already exists
    """
    @patch('ToDoListProject.myServices.UserService.UserService.get_user_by_id')
    @patch('ToDoListProject.myServices.UserService.UserService.get_list_by_user_id')
    def test_create_list_not_possible_because_list_already_exists(self, mock_get_list_by_user_id, mock_get_user_by_id):
        mock_get_list_by_user_id.return_value = True  # list already exist
        mock_get_user_by_id.return_value = True  # the user exists
        # without mocking because I had some issues with user's mock and database
        u2 = User(firstname="Toto", lastname="Tata", password="totototo", birthdate=datetime.date(2000, 1, 1),
                  email="toto@tata.fr")
        u2.save()
        user_list = self.user_service.create_one_list_for_user(u2)
        self.assertIsNone(user_list)