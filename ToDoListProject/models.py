import re
from datetime import date

from django.db import models


class User(models.Model):
    firstname = models.CharField(max_length=40)
    lastname = models.CharField(max_length=40)
    password = models.CharField(max_length=40)
    birthdate = models.DateField()
    email = models.EmailField()

    def is_valid(self):
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w+$'

        if self.email is None \
                or self.firstname is None \
                or self.lastname is None \
                or self.birthdate is None \
                or self.password is None:
            print("something missing")
            return False

        if re.search(regex, self.email) is None:
            return False

        today = date.today()
        age = today.year - self.birthdate.year - ((today.month, today.day) < (self.birthdate.month, self.birthdate.day))
        if age < 13:
            return False

        if len(self.password) < 8 or len(self.password) > 40:
            return False

        return True


class ToDoList(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)


class Item(models.Model):
    name = models.CharField(max_length=20)
    content = models.CharField(max_length=1000)
    list = models.ForeignKey('ToDoList', on_delete=models.CASCADE)
    creation_date = models.DateTimeField(default=None)

    def is_valid(self):
        if self.name is None or self.content is None :
            return False
        return True
